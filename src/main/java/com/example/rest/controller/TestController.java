package com.example.rest.controller;

import com.example.rest.model.User;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Collections;

/**
 * @author magiccrafter
 */
@RestController
public class TestController {

    @GetMapping(value = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
    public void test() {
        throw new ConstraintViolationException("Empty String ", Collections.emptySet());
    }

    @GetMapping(value = "/test1", produces = MediaType.APPLICATION_JSON_VALUE)
    public void test1()  {
      Integer a = null;
      if (a == null) {
          throw new NullPointerException("Null value");
      }
      a = a +1;
    }

    @RequestMapping(value = "/test2",method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    public void testPost( @Valid @RequestBody User user) {
        System.out.println(user.toString());
    }
}
