package com.example.rest.error;

import java.util.List;

/**
 * @author magiccrafter
 */
public class ApiErrorResponse {

    private int status;
    private int code;
    private List<ApiError> messages;

    public ApiErrorResponse(int status, int code, List<ApiError> messages) {
        this.status = status;
        this.code = code;
        this.messages = messages;
    }

    public int getStatus() {
        return status;
    }

    public int getCode() {
        return code;
    }

    public List<ApiError> getMessage() {
        return messages;
    }

    @Override
    public String toString() {
        return "ApiErrorResponse{" +
                "status=" + status +
                ", code=" + code +
                ", message=" + messages.toString()  +
                '}';
    }
}
