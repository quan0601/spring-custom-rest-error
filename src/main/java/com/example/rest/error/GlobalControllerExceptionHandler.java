package com.example.rest.error;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author magiccrafter
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(value = { ConstraintViolationException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrorResponse constraintViolationException(ConstraintViolationException ex) {
        List<ApiError> messages =  new ArrayList<>();
        List<ConstraintViolation> errors = new ArrayList<>(ex.getConstraintViolations());
        for(ConstraintViolation each : errors) {

            ApiError error = new ApiError("error",each.getMessage());
            messages.add(error);
        }
        return new ApiErrorResponse(406, 4006, messages);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrorResponse MethodArgumentNotValidException(MethodArgumentNotValidException ex)    {

        List<ApiError> rsError = new ArrayList<>();
        List<FieldError> errors = ex.getBindingResult().getFieldErrors();
        for(FieldError error:errors) {
            rsError.add(new ApiError(error.getField(),error.getDefaultMessage()));
        }

        return new ApiErrorResponse(406, 4006,rsError );
    }

    @ExceptionHandler(value = { NoHandlerFoundException.class })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiErrorResponse noHandlerFoundException(NoHandlerFoundException ex) {
        List<ApiError> messages =  new ArrayList<>();
        messages.add(new ApiError("Not Found at request " +ex.getRequestURL(),ex.getMessage()));
        return new ApiErrorResponse(404, 4041,messages);
    }

    @ExceptionHandler(value = { Exception.class })
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiErrorResponse unknownException(Exception ex) {
        List<ApiError> messages =  new ArrayList<>();
        messages.add(new ApiError("Execption error",ex.getMessage()));
        return new ApiErrorResponse(500, 5002, messages);
    }
}
