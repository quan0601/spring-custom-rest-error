package com.example.rest.model;


import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class User {
    @Override
    public String toString() {
        return  "id = " + this.id + " age=" + this.age;
    }

    @NotNull(message = "{error.message.notnull}")
    private String id;

    @NotNull(message = "NOT NULL")
    @Range(min = 1,max = 90,message = "{error.message.age}")
    private Integer age;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
